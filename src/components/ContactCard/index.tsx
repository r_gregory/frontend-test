import React, { FC } from "react";
import { Link as RouterLink } from "react-router-dom";
import {
  Card,
  CardActions,
  CardContent,
  Typography,
  Button
} from "@material-ui/core";

type Props = {
  id: string;
  name: string;
  email: string;
  onDelete: (id: string) => void;
};

const ContactCard: FC<Props> = ({ id, name, email, onDelete }) => (
  <div>
    <CardContent>
      <Typography variant="subtitle1">{name}</Typography>
      <Typography variant="body1">{email}</Typography>
    </CardContent>
    <CardActions>
      <Button
        component={RouterLink}
        variant="outlined"
        to={`/contact/${id}/edit`}
      >
        Edit
      </Button>
      <Button variant="outlined" onClick={() => onDelete(id)}>
        Delete
      </Button>
    </CardActions>
  </div>
);

export default ContactCard;
