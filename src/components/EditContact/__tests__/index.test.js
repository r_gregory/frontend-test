import React from "react";
import {
  waitFor,
  fireEvent,
  renderWithRouter,
  waitForElementToBeRemoved
} from "test-utils";
import { MockedProvider } from "@apollo/react-testing";
import EditContact from "..";
import { UPDATE_CONTACT_MUTATION, CONTACT_QUERY } from "../../../queries";

describe("EditContact", () => {
  it("Should load contact and prefill form", async () => {
    const { getByRole, getByTestId } = renderWithRouter(
      <MockedProvider mocks={contactMock} addTypename={false}>
        <EditContact />
      </MockedProvider>,
      {
        path: "/contact/:id/edit",
        initialEntries: ["/contact/1234/edit"]
      }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(getByRole("textbox", { name: "Name *" }).value).toBe("Joe");
    expect(getByRole("textbox", { name: "Email *" }).value).toBe(
      "joe@bloggs.com"
    );
  });

  it("Should show alert when error loading contact", async () => {
    const { getByTestId, queryByRole, getByRole } = renderWithRouter(
      <MockedProvider mocks={errorMock} addTypename={false}>
        <EditContact />
      </MockedProvider>,
      {
        path: "/contact/:id/edit",
        initialEntries: ["/contact/1234/edit"]
      }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(getByRole("alert")).toBeInTheDocument();
    expect(queryByRole("textbox", { name: "Email *" })).not.toBeInTheDocument();
  });

  it("Should show alert when no contact found", async () => {
    const { getByTestId, getByRole } = renderWithRouter(
      <MockedProvider mocks={noContactMock} addTypename={false}>
        <EditContact />
      </MockedProvider>,
      {
        path: "/contact/:id/edit",
        initialEntries: ["/contact/1234/edit"]
      }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(getByRole("alert")).toBeInTheDocument();
  });

  it("Should update contact successfully and redirect to /", async () => {
    const { history, getByRole, getByTestId } = renderWithRouter(
      <MockedProvider mocks={updateContactMock} addTypename={false}>
        <EditContact />
      </MockedProvider>,
      {
        path: "/contact/:id/edit",
        initialEntries: ["/contact/1234/edit"]
      }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    fireEvent.change(getByRole("textbox", { name: "Name *" }), {
      target: { value: "Joe Updated" }
    });
    fireEvent.click(getByRole("button", { name: "Submit" }));

    await waitForElementToBeRemoved(getByTestId("contact-form"));
    expect(history.location.pathname).toEqual("/");
  });

  it("Should show alert when error updating contact", async () => {
    const { getByRole, getByTestId } = renderWithRouter(
      <MockedProvider mocks={deleteErrorMock} addTypename={false}>
        <EditContact />
      </MockedProvider>,
      {
        path: "/contact/:id/edit",
        initialEntries: ["/contact/1234/edit"]
      }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    fireEvent.change(getByRole("textbox", { name: "Name *" }), {
      target: { value: "Joe Updated" }
    });
    fireEvent.click(getByRole("button", { name: "Submit" }));
    await waitFor(() => expect(getByRole("alert")).toBeInTheDocument());
  });
});

const contact = {
  id: "1234",
  name: "Joe",
  email: "joe@bloggs.com"
};

const contactMock = [
  {
    request: {
      query: CONTACT_QUERY,
      variables: { id: contact.id }
    },
    result: { data: { contact } }
  }
];

const errorMock = [
  {
    request: {
      query: CONTACT_QUERY
    },
    result: new Error("Bad request")
  }
];

const noContactMock = [
  {
    request: {
      query: CONTACT_QUERY,
      variables: { id: contact.id }
    },
    result: { data: { contact: null } }
  }
];

const updateContactMock = [
  {
    request: {
      query: CONTACT_QUERY,
      variables: { id: contact.id }
    },
    result: { data: { contact } }
  },
  {
    request: {
      query: UPDATE_CONTACT_MUTATION,
      variables: {
        contact: {
          ...contact,
          name: "Joe Updated"
        }
      }
    },
    result: {
      data: {
        updateContact: {
          ...contact,
          name: "Joe Updated"
        }
      }
    }
  }
];

const deleteErrorMock = [
  {
    request: {
      query: CONTACT_QUERY,
      variables: { id: contact.id }
    },
    result: { data: { contact } }
  },
  {
    request: {
      query: UPDATE_CONTACT_MUTATION,
      variables: {
        contact: {
          ...contact,
          name: "Joe Updated"
        }
      }
    },
    error: new Error("Bad Request")
  }
];
