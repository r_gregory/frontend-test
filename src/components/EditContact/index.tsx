import React, { FC, Fragment } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { useParams, useHistory } from "react-router-dom";
import { Alert, Skeleton } from "@material-ui/lab";
import { CONTACT_QUERY, UPDATE_CONTACT_MUTATION } from "../../queries";
import { IContact } from "../../types";
import ContactForm from "../ContactForm";

const EditContact: FC = () => {
  const { id } = useParams();
  const history = useHistory();
  const { data, loading, error } = useQuery(CONTACT_QUERY, {
    variables: { id }
  });
  const [
    updateContact,
    { loading: updateLoading, error: updateError }
  ] = useMutation(UPDATE_CONTACT_MUTATION, {
    onCompleted: () => history.push("/"),
    onError: () => {}
  });

  const handleSubmit = (contact: IContact) => {
    updateContact({
      variables: {
        contact: {
          id: contact.id,
          name: contact.name,
          email: contact.email
        }
      }
    });
  };

  if (loading) {
    return (
      <div data-testid="loading">
        <Skeleton variant="text" animation="wave" height={40} />
        <Skeleton variant="text" animation="wave" height={40} />
      </div>
    );
  }

  if (error) {
    return <Alert severity="error">Error loading contact</Alert>;
  }

  return (
    <Fragment>
      {data && data.contact ? (
        <ContactForm
          initialValues={data.contact}
          onSubmit={handleSubmit}
          submitting={updateLoading}
          submissionError={updateError ? "Error updating contact" : undefined}
        />
      ) : (
        <Alert severity="error">No contact found</Alert>
      )}
    </Fragment>
  );
};

export default EditContact;
