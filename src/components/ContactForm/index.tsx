import React, { FC, useState } from "react";
import { TextField, Button, makeStyles } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { isEmpty, validateForm } from "./validation";

type Props = {
  initialValues?: Contact;
  onSubmit: Function;
  submitting: boolean;
  submissionError?: string;
};

interface Contact {
  id?: string;
  name: string;
  email: string;
}

export interface FormValues {
  name: string;
  email: string;
}

const useStyles = makeStyles(() => ({
  button: {
    marginTop: "20px"
  }
}));

const ContactForm: FC<Props> = ({
  initialValues = {},
  onSubmit,
  submitting,
  submissionError
}) => {
  const classes = useStyles();
  const [formErrors, setFormErrors] = useState<string[]>([]);
  const [formValues, setFormValues] = useState<FormValues>({
    ...{ name: "", email: "" },
    ...initialValues
  });

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const errors = validateForm(formValues);

    if (errors.length) {
      return setFormErrors(errors);
    }

    onSubmit(formValues);
  };

  const handleChange = (
    e: React.ChangeEvent<{ value: unknown; name: string }>
  ) => {
    setFormValues({
      ...formValues,
      [e.currentTarget.name]: e.currentTarget.value
    });
  };

  const handleBlur = (
    e: React.ChangeEvent<{ value: string; name: string }>
  ) => {
    const { currentTarget } = e;
    const { name, value } = currentTarget;

    if (isEmpty(value)) {
      setFormErrors([...formErrors, name]);
    } else {
      setFormErrors(formErrors.filter(error => error !== name));
    }
  };

  return (
    <form
      onSubmit={handleSubmit}
      noValidate
      autoComplete="off"
      data-testid="contact-form"
    >
      {submissionError && <Alert severity="error">{submissionError}</Alert>}
      <TextField
        id="name"
        label="Name"
        type="text"
        name="name"
        value={formValues.name}
        onChange={handleChange}
        onBlur={handleBlur}
        required
        fullWidth
        margin="normal"
        error={formErrors.includes("name")}
        helperText={formErrors.includes("name") && "Required"}
        data-testid="name-input"
      />
      <TextField
        id="email"
        label="Email"
        type="text"
        name="email"
        value={formValues.email}
        onChange={handleChange}
        onBlur={handleBlur}
        required
        fullWidth
        margin="normal"
        error={formErrors.includes("email")}
        helperText={formErrors.includes("email") && "Required"}
        data-testid="email-input"
      />
      <Button
        className={classes.button}
        type="submit"
        disabled={submitting}
        variant="outlined"
      >
        Submit
      </Button>
    </form>
  );
};

export default ContactForm;
