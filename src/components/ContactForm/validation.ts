import { FormValues } from ".";

export const isEmpty = (value: string): boolean => {
  if (!value || value.trim() === "") {
    return true;
  }
  return false;
};

export const validateForm = (values: FormValues) => {
  const errors: string[] = [];

  (Object.keys(values) as Array<keyof typeof values>).forEach(key => {
    if (isEmpty(values[key])) {
      errors.push(key);
    }
  });

  return errors;
};
