import React from "react";
import { render, fireEvent } from "test-utils";
import ContactForm from "../";

describe("ContactForm", () => {
  it("Should validate inputs on blur", () => {
    const { getByRole, getAllByText } = render(
      <ContactForm onSubmit={jest.fn} />
    );

    fireEvent.blur(getByRole("textbox", { name: "Name *" }), {
      target: { value: "" }
    });
    fireEvent.blur(getByRole("textbox", { name: "Email *" }), {
      target: { value: "" }
    });

    expect(getAllByText("Required").length).toBe(2);

    fireEvent.blur(getByRole("textbox", { name: "Name *" }), {
      target: { value: "test" }
    });

    expect(getAllByText("Required").length).toBe(1);
  });

  it("Should validate on submit", () => {
    const { getByRole, getAllByText } = render(
      <ContactForm onSubmit={jest.fn} />
    );

    fireEvent.click(getByRole("button"));

    expect(getAllByText("Required").length).toBe(2);
  });
});
