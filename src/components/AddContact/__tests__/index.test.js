import React from "react";
import { renderWithRouter, waitFor, fireEvent } from "test-utils";
import { MockedProvider } from "@apollo/react-testing";
import { ADD_CONTACT_MUTATION, CONTACTS_QUERY } from "../../../queries";
import AddContact from "../";

describe("AddContact", () => {
  it("Should add contact succesfully and redirect to /", async () => {
    const { history, getByRole } = renderWithRouter(
      <MockedProvider mocks={addContactMock} addTypename={false}>
        <AddContact />
      </MockedProvider>,
      {
        path: "/add",
        initialEntries: ["/add"]
      }
    );

    const nameInput = getByRole("textbox", { name: "Name *" });
    const emailInput = getByRole("textbox", { name: "Email *" });
    const submitButton = getByRole("button", { name: "Submit" });

    fireEvent.change(nameInput, { target: { value: "Joe" } });
    fireEvent.change(emailInput, { target: { value: "joe@bloggs.com" } });
    fireEvent.submit(submitButton);

    await waitFor(() => expect(nameInput).not.toBeInTheDocument());

    expect(history.location.pathname).toBe("/");
  });

  it("Should show alert when error adding contact", async () => {
    const { getByRole } = renderWithRouter(
      <MockedProvider mocks={addContactErrorMock} addTypename={false}>
        <AddContact />
      </MockedProvider>,
      {
        path: "/add",
        initialEntries: ["/add"]
      }
    );

    const nameInput = getByRole("textbox", { name: "Name *" });
    const emailInput = getByRole("textbox", { name: "Email *" });
    const submitButton = getByRole("button", { name: "Submit" });

    fireEvent.change(nameInput, { target: { value: "Joe" } });
    fireEvent.change(emailInput, { target: { value: "joe@bloggs.com" } });
    fireEvent.submit(submitButton);

    await waitFor(() => expect(getByRole("alert")).toBeInTheDocument());
  });
});

const contact = {
  id: "1234",
  name: "Joe",
  email: "joe@bloggs.com"
};

export const addContactMock = [
  {
    request: {
      query: ADD_CONTACT_MUTATION,
      variables: {
        contact: {
          name: "Joe",
          email: "joe@bloggs.com"
        }
      }
    },
    result: { data: { addContact: contact } }
  },
  {
    request: {
      query: CONTACTS_QUERY
    },
    result: {
      data: { contacts: [contact] }
    }
  }
];

export const addContactErrorMock = [
  {
    request: {
      query: ADD_CONTACT_MUTATION,
      variables: {
        contact: {
          name: "Joe",
          email: "joe@bloggs.com"
        }
      }
    },
    error: new Error("Bad request")
  }
];
