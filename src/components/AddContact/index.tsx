import React, { FC, Fragment } from "react";
import { useMutation } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import { IContact } from "../../types";
import { ADD_CONTACT_MUTATION, CONTACTS_QUERY } from "../../queries";
import ContactForm from "../ContactForm";

const AddContact: FC = () => {
  const history = useHistory();
  const [addContact, { loading, error }] = useMutation(ADD_CONTACT_MUTATION, {
    refetchQueries: [{ query: CONTACTS_QUERY }],
    onCompleted: () => history.push("/"),
    onError: () => {}
  });

  const handleSubmit = (contact: IContact) => {
    addContact({
      variables: { contact }
    });
  };

  return (
    <Fragment>
      <ContactForm
        onSubmit={handleSubmit}
        submitting={loading}
        submissionError={error ? "Error adding contact" : undefined}
      />
    </Fragment>
  );
};

export default AddContact;
