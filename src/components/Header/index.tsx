import React, { FC } from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Link,
  makeStyles,
  Theme
} from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";

const useStyles = makeStyles<Theme>((theme: Theme) => ({
  menuItem: {
    marginRight: theme.spacing(2)
  },
  title: {
    marginRight: theme.spacing(4)
  }
}));

const Header: FC = () => {
  const classes = useStyles();

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h5" className={classes.title}>
          <Link component={RouterLink} color="inherit" to="/">
            Contacts
          </Link>
        </Typography>
        <Typography>
          <Link
            className={classes.menuItem}
            component={RouterLink}
            color="inherit"
            to="/contact/add"
          >
            Add Contact
          </Link>
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
