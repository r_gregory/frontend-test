/* eslint-disable import/first */
jest.mock("../../ContactsList", () => () => <div>Contacts List</div>);
jest.mock("../../ViewContact", () => () => <div>Contact</div>);
jest.mock("../../AddContact", () => () => <div>Add Contact Form</div>);
jest.mock("../../EditContact", () => () => <div>Edit Contact</div>);

import React from "react";
import { render } from "test-utils";
import { MemoryRouter } from "react-router-dom";
import App from "../";

describe("App", () => {
  it("Should route to Contact List", () => {
    const { getByText } = render(
      <MemoryRouter initialEntries={["/"]}>
        <App />
      </MemoryRouter>
    );

    expect(getByText("Contacts List").toBeInTheDocument);
  });

  it("Should route to Contact", () => {
    const { getByText } = render(
      <MemoryRouter initialEntries={["/contact/1"]}>
        <App />
      </MemoryRouter>
    );

    expect(getByText("Contact").toBeInTheDocument);
  });

  it("Should route to Add Contact", () => {
    const { getByText } = render(
      <MemoryRouter initialEntries={["/contact/add"]}>
        <App />
      </MemoryRouter>
    );

    expect(getByText("Add Contact Form").toBeInTheDocument);
  });

  it("Should route to Edit Contact", () => {
    const { getByText } = render(
      <MemoryRouter initialEntries={["/contact/1/edit"]}>
        <App />
      </MemoryRouter>
    );

    expect(getByText("Edit Contact").toBeInTheDocument);
  });
});
