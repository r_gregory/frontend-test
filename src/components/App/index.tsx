import React, { FC } from "react";
import { Route, Switch } from "react-router-dom";
import { Container, Paper, makeStyles, Theme } from "@material-ui/core";
import Header from "../Header";
import ContactsList from "../ContactsList";
import ViewContact from "../ViewContact";
import EditContact from "../EditContact";
import AddContact from "../AddContact";

const useStyles = makeStyles<Theme>((theme: Theme) => ({
  paper: {
    padding: theme.spacing(3)
  }
}));

const App: FC = () => {
  const classes = useStyles();

  return (
    <Container maxWidth="sm">
      <Header />
      <Paper className={classes.paper}>
        <Switch>
          <Route exact path="/">
            <ContactsList />
          </Route>
          <Route exact path="/contact/add">
            <AddContact />
          </Route>
          <Route exact path="/contact/:id">
            <ViewContact />
          </Route>
          <Route exact path="/contact/:id/edit">
            <EditContact />
          </Route>
        </Switch>
      </Paper>
    </Container>
  );
};

export default App;
