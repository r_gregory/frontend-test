import React from "react";
import {
  waitFor,
  waitForElementToBeRemoved,
  fireEvent,
  renderWithRouter
} from "test-utils";
import { MockedProvider } from "@apollo/react-testing";
import ViewContact from "../";
import {
  CONTACTS_QUERY,
  CONTACT_QUERY,
  DELETE_CONTACT_MUTATION
} from "../../../queries";

describe("ContactList", () => {
  it("Should load and display a contact", async () => {
    const { container, getByTestId } = renderWithRouter(
      <MockedProvider mocks={contactMock} addTypename={false}>
        <ViewContact />
      </MockedProvider>,
      { path: "/contact/:id", initialEntries: ["/contact/1234"] }
    );

    expect(getByTestId("loading")).toBeInTheDocument();
    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(container).toMatchSnapshot();
  });

  it("Should show error message when response error returned", async () => {
    const { container, getByTestId } = renderWithRouter(
      <MockedProvider mocks={errorMock} addTypename={false}>
        <ViewContact />
      </MockedProvider>,
      { path: "/contact/:id", initialEntries: ["/contact/1234"] }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(container).toMatchSnapshot();
  });

  it("Should show info message when no contact found", async () => {
    const { container, getByTestId } = renderWithRouter(
      <MockedProvider mocks={noContactMock} addTypename={false}>
        <ViewContact />
      </MockedProvider>,
      { path: "/contact/:id", initialEntries: ["/contact/123"] }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(container).toMatchSnapshot();
  });

  it("Should delete contact and redirect to /", async () => {
    const { getByText, queryByText, getByTestId, history } = renderWithRouter(
      <MockedProvider mocks={deleteMock} addTypename={false}>
        <ViewContact />
      </MockedProvider>,
      { path: "/contact/:id", initialEntries: ["/contact/1234"] }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    fireEvent.click(getByText("Delete"));

    await waitFor(() => !queryByText("Jane Doe"));
    expect(history.location.pathname).toBe("/");
  });

  it("Should show delete error when delete contact fails", async () => {
    const { container, getByText, getByRole, getByTestId } = renderWithRouter(
      <MockedProvider mocks={deleteErrorMock} addTypename={false}>
        <ViewContact />
      </MockedProvider>,
      { path: "/contact/:id", initialEntries: ["/contact/1234"] }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    fireEvent.click(getByText("Delete"));

    await waitFor(() => expect(getByRole("alert")).toBeInTheDocument());
    expect(container).toMatchSnapshot();
  });
});

const contactMock = [
  {
    request: {
      query: CONTACT_QUERY,
      variables: { id: "1234" }
    },
    result: {
      data: {
        contact: { id: "1234", name: "Jane Doe", email: "jane@doe.com" }
      }
    }
  }
];

const errorMock = [
  {
    request: {
      query: CONTACT_QUERY
    },
    error: new Error("Bad request")
  }
];

const noContactMock = [
  {
    request: {
      query: CONTACT_QUERY,
      variables: { id: "123" }
    },
    result: {
      data: { contact: null }
    }
  }
];

const deleteMock = [
  {
    request: {
      query: CONTACT_QUERY,
      variables: { id: "1234" }
    },
    result: {
      data: {
        contact: { id: "1234", name: "Jane Doe", email: "jane@doe.com" }
      }
    }
  },
  {
    request: {
      query: DELETE_CONTACT_MUTATION,
      variables: { id: "1234" }
    },
    result: {
      data: {
        deleteContact: true
      }
    }
  },
  {
    request: {
      query: CONTACTS_QUERY
    },
    result: {
      data: {
        contacts: []
      }
    }
  }
];

const deleteErrorMock = [
  {
    request: {
      query: CONTACT_QUERY,
      variables: { id: "1234" }
    },
    result: {
      data: {
        contact: { id: "1234", name: "Jane Doe", email: "jane@doe.com" }
      }
    }
  },
  {
    request: {
      query: DELETE_CONTACT_MUTATION,
      variables: { id: "1234" }
    },
    error: new Error("Bad request")
  }
];
