import React, { FC, Fragment } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { useParams, useHistory } from "react-router-dom";
import { Alert, Skeleton } from "@material-ui/lab";
import ContactCard from "../ContactCard";
import {
  CONTACTS_QUERY,
  CONTACT_QUERY,
  DELETE_CONTACT_MUTATION
} from "../../queries";

const ViewContact: FC = () => {
  const { id } = useParams();
  const history = useHistory();
  let { data, loading, error } = useQuery(CONTACT_QUERY, {
    variables: { id }
  });
  const [deleteContact, { error: deleteError }] = useMutation(
    DELETE_CONTACT_MUTATION,
    {
      refetchQueries: [{ query: CONTACTS_QUERY }],
      onCompleted: () => history.push("/"),
      onError: () => {}
    }
  );

  const handleDelete = (id: string) => {
    deleteContact({
      variables: { id }
    });
  };

  if (loading) {
    return (
      <div data-testid="loading">
        <Skeleton variant="text" animation="wave" height={35} />
        <Skeleton variant="text" animation="wave" height={35} />
        <Skeleton variant="text" animation="wave" height={80} width={100} />
      </div>
    );
  }

  if (error) {
    return <Alert severity="error">Error loading contact</Alert>;
  }

  return (
    <Fragment>
      {data && data.contact ? (
        <Fragment>
          {deleteError && (
            <Alert severity="error">Error deleting contact</Alert>
          )}
          <ContactCard {...data.contact} onDelete={handleDelete} />
        </Fragment>
      ) : (
        <Alert severity="error">No contact found</Alert>
      )}
    </Fragment>
  );
};

export default ViewContact;
