import React, { FC, Fragment } from "react";
import { useQuery } from "@apollo/react-hooks";
import { List, ListItem, ListItemText, ListItemIcon } from "@material-ui/core";
import { Alert, Skeleton } from "@material-ui/lab";
import PersonIcon from "@material-ui/icons/Person";
import { Link as RouterLink } from "react-router-dom";
import { IContacts } from "../../types";
import { CONTACTS_QUERY } from "../../queries";

const ContactsList: FC = () => {
  let { data = { contacts: [] }, loading, error } = useQuery<IContacts>(
    CONTACTS_QUERY
  );

  if (loading) {
    return (
      <div data-testid="loading">
        <Skeleton variant="text" animation="wave" height={70} />
        <Skeleton variant="text" animation="wave" height={70} />
        <Skeleton variant="text" animation="wave" height={70} />
      </div>
    );
  }

  if (error) {
    return <Alert severity="error">Error loading contacts</Alert>;
  }

  if (!data.contacts.length) {
    return <Alert severity="info">There are no contacts</Alert>;
  }

  return (
    <List>
      {data.contacts.map(contact => (
        <ListItem
          key={contact.id}
          button
          component={RouterLink}
          to={`/contact/${contact.id}`}
        >
          <ListItemIcon>
            <PersonIcon />
          </ListItemIcon>
          <ListItemText primary={contact.name} />
        </ListItem>
      ))}
    </List>
  );
};

export default ContactsList;
