import React from "react";
import { render, waitForElementToBeRemoved } from "test-utils";
import { MemoryRouter } from "react-router-dom";
import { MockedProvider } from "@apollo/react-testing";
import { CONTACTS_QUERY } from "../../../queries";
import ContactList from "../";

describe("ContactList", () => {
  it("Should load a list of contacts", async () => {
    const { container, getByTestId } = render(
      <MockedProvider mocks={contactsMock} addTypename={false}>
        <ContactList />
      </MockedProvider>,
      { wrapper: MemoryRouter }
    );

    expect(getByTestId("loading")).toBeInTheDocument();
    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(container).toMatchSnapshot();
  });

  it("Should show error message when response error returned", async () => {
    const { container, getByTestId } = render(
      <MockedProvider mocks={errorMock} addTypename={false}>
        <ContactList />
      </MockedProvider>,
      { wrapper: MemoryRouter }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(container).toMatchSnapshot();
  });

  it("Should show info message when empty contacts list", async () => {
    const { container, getByTestId } = render(
      <MockedProvider mocks={noContactsMock} addTypename={false}>
        <ContactList />
      </MockedProvider>,
      { wrapper: MemoryRouter }
    );

    await waitForElementToBeRemoved(getByTestId("loading"));
    expect(container).toMatchSnapshot();
  });
});

const contactsMock = [
  {
    request: {
      query: CONTACTS_QUERY
    },
    result: {
      data: {
        contacts: [{ id: "1234", name: "Jane Doe", email: "jane@doe.com" }]
      }
    }
  }
];

const errorMock = [
  {
    request: {
      query: CONTACTS_QUERY
    },
    error: new Error("Bad request")
  }
];

const noContactsMock = [
  {
    request: {
      query: CONTACTS_QUERY
    },
    result: {
      data: {
        contacts: []
      }
    }
  }
];
