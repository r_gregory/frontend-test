import { gql } from "apollo-boost";

export const CONTACTS_QUERY = gql`
  query {
    contacts {
      id
      name
      email
    }
  }
`;

export const CONTACT_QUERY = gql`
  query Contact($id: ID!) {
    contact(id: $id) {
      id
      name
      email
    }
  }
`;

export const UPDATE_CONTACT_MUTATION = gql`
  mutation UpdateContact($contact: InputContact) {
    updateContact(contact: $contact) {
      id
      name
      email
    }
  }
`;

export const ADD_CONTACT_MUTATION = gql`
  mutation AddContact($contact: InputContact) {
    addContact(contact: $contact) {
      id
      name
      email
    }
  }
`;

export const DELETE_CONTACT_MUTATION = gql`
  mutation DeleteContact($id: ID) {
    deleteContact(id: $id)
  }
`;
