import React from "react";
import { Route, Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

const renderWithRouter = (component, { path, initialEntries = [] } = {}) => {
  const history = createMemoryHistory({ initialEntries });
  const Wrapper = ({ children }) => (
    <Router history={history}>
      <Route path={path}>{children}</Route>
    </Router>
  );

  return {
    ...render(component, { wrapper: Wrapper }),
    history
  };
};

export * from "@testing-library/react";
export { renderWithRouter };
