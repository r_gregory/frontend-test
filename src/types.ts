export interface IContacts {
  contacts: Array<IContact>;
}

export interface IContact {
  id: string;
  name: string;
  email: string;
}
